#include<iostream>
#include<vector>
#include"cxx-prettyprint.hh"

enum class TokenType {Eof, V, Id, F, I, Str, Err, Sp};
std::ostream& operator<< (std::ostream& out, const TokenType& t) {
  switch (t) {
  case TokenType::Eof	: out << "EOF"; break;
  case TokenType::V		: out << "Verb"; break;
  case TokenType::F		: out << "Float"; break;
  case TokenType::I		: out << "Int"; break;
  case TokenType::Id	: out << "Identifier"; break;
  case TokenType::Str	: out << "Str"; break;
  case TokenType::Err	: out << "Error"; break;
  case TokenType::Sp	: out << "Special"; break;
  default: break;
  }
  return out;
}

class Token {
public: virtual TokenType get_type() const = 0; virtual ~Token() = default; size_t get_line() { return line; } size_t get_col() { return col; } virtual void print(std::ostream& os) const = 0;
protected: size_t line; size_t col; Token(size_t line, size_t col) : line{line}, col{col} {}};
std::ostream& operator<<(std::ostream& str, const Token& tok) {tok.print(str); return str;}

template<TokenType x, typename val>
class TokenWrapper: public Token {
public: TokenWrapper(val v, size_t line, size_t col) : Token(line, col), value{v} {} val& get_value() const { return &value; }; virtual TokenType get_type() const override { return x; };
  virtual void print(std::ostream& os) const override { os << "[" << get_type() << " (" << line << ":" << col << ")] " << value;  };
private: val value; };

typedef TokenWrapper<TokenType::Err, std::string> err_token;
const std::string VERBS[] = {"+", "-", "*", "%", "!", "#", "_", "<-", "->", ":", "'", "/", "\\"};

class Lexer {
private: std::string_view src; size_t line; size_t col;
  void accept(size_t len);
public:
  Lexer(std::string_view src) { this->src = src; this->line=0; this->col=0; }
  std::unique_ptr<Token> next();
};

void Lexer::accept(size_t len) {
  for (char c : src.substr(0, len)) { if (c == '\n') { line++; col = 0;} else if (c != '\r') col++; }
  src.remove_prefix(len);
}

std::unique_ptr<Token> Lexer::next() {
  accept(std::min(src.find_first_not_of("\t\n\r "), src.length()));

  if (src.empty()) return std::make_unique<TokenWrapper<TokenType::Eof, int>>(0, line, col);

  if (src.front() == '"') {
	size_t len = 0;
	bool skip = true;
	for (char c : src) { len++;
	  if (skip) { skip = false; }
	  else if (c == '\\') { skip = true; }
	  else if (c == '"') {
		auto t = std::make_unique<TokenWrapper<TokenType::Str, std::string>>(std::string(src.substr(1, len-2)), line, col);
		accept(len);
		return t;
	  }}
	accept(src.length());
	return std::make_unique<TokenWrapper<TokenType::Err, std::string>>("Unterminated string", line, col);
  }

  if (src.front() == '`') {
	size_t len = std::min(src.find_first_of("\n\t\r "), src.length());
	if (src.substr(1, len).find('`') == std::string_view::npos) {
	  auto t = std::make_unique<TokenWrapper<TokenType::Str, std::string>>(std::string(src.substr(1, len)), line, col);
	  accept(len);
	  return t;
	} else {
	  std::string_view x = src.substr(0, len);
	  std::vector<std::string> v = std::vector<std::string>();

	  while (!x.empty()) {
		x.remove_prefix(1);
		std::string_view sym = x.substr(0, std::min(x.length(), x.find('`')));
		v.push_back(std::string(sym));
		x.remove_prefix(sym.length());
	  }

	  auto t = std::make_unique<TokenWrapper<TokenType::Str, std::vector<std::string>>>(v, line, col);
	  accept(len);
	  return t;
	}
  }

  for (auto& v: VERBS) { if (v==src.substr(0, v.length())) { auto t = std::make_unique<TokenWrapper<TokenType::V, std::string>>(std::string(v), line, col); accept(v.length()); return t; } }

  if (std::isdigit((src.front()))) {
	size_t len = std::min(src.find_first_not_of("0123456789. "), src.length());
	std::string_view s = src.substr(0, len);
	std::unique_ptr<Token> t;
	if (std::string_view::npos == s.find('.')) {

	  auto v = std::vector<int64_t>();
	  while (!s.empty()) {
		s.remove_prefix(std::min(s.length(), s.find_first_not_of(" ")));
		std::string_view chunk = s.substr(0, std::min(s.find(' '), s.length()));
		if(chunk.empty()) break;
		v.push_back(std::stoi(std::string(chunk)));
		s.remove_prefix(chunk.length());
	  }

	  t = std::make_unique<TokenWrapper<TokenType::I, std::vector<int64_t>>>(v, line, col);
	} else {

	  auto v = std::vector<double>();
	  while (!s.empty()) {
		s.remove_prefix(std::min(s.length(), s.find_first_not_of(" ")));
		std::string_view chunk = s.substr(0, std::min(s.find(' '), s.length()));
		if (chunk.empty()) break;
		v.push_back(std::stod(std::string(chunk)));
		s.remove_prefix(chunk.length());
	  }
	  t = std::make_unique<TokenWrapper<TokenType::F, std::vector<double>>>(v, line, col);
	}

	accept(len);
	return t;
  }

  if (src.front() == '(' || src.front() == ')' || src.front() == ';' || src.front() == '[' || src.front() == ']' || src.front() == '{' || src.front() == '}') {
	auto t = std::make_unique<TokenWrapper<TokenType::Sp, char>>(src.front(), line, col);
	accept(1);
	return t;
  }

  return std::make_unique<TokenWrapper<TokenType::Err, std::string>>("Unmatched character", line, col);
}

int main() {
  for (std::string line; std::getline(std::cin, line); ) {
	Lexer l = Lexer(line);
	for (auto t = l.next(); t->get_type() != TokenType::Eof; t = l.next())
	  std::cout << *t << std::endl;
  }
}
